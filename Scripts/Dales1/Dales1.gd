extends Node

var score = 0
export var next_level = "res://Scenes/Dales2.tscn"

signal to_next_level

func _ready():
	#Set the score per level
	score = get_node("/root/Global").total_score




func _on_CoinArea_body_entered(body):
	if body.name == "Player":
		score += 100


func _on_EndLevel_body_entered(body):
	#Doesn't run with music because that doesn't work in browser
	_on_music_finished()
	#var player = AudioStreamPlayer.new()
	#self.add_child(player)
	#player.connect("finished", self, "_on_music_finished")
	#player.stream = get_node("/root/Global").yorkshire
	#player.stream.loop = 0
	#player.play()
	
	#player.pause_mode = 2
	#get_tree().paused = true
	
	
	
	


func _on_music_finished():
	get_node("/root/Global").total_score = score
	get_tree().paused = false
	get_tree().change_scene(next_level)


func _on_Body_body_entered(body):
	#Died
	if body.name == "Player":
		print("You died")
		get_tree().paused = true


func _on_Enemy_enemy_killed():
	print("Killed")
	score += 100
