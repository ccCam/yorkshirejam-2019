extends Node

var score = 0
const next_level = "res://Scenes/Dales3.tscn"

signal to_next_level

func _ready():
	score = get_root().total_score




func _on_CoinArea_body_entered(body):
	score += 100


func _on_EndLevel_body_entered(body):
	emit_signal("to_next_level", next_level, score)


func _on_Body_body_entered(body):
	#Died
	if body.name == "Player":
		print("You died")
		get_tree().paused = true
