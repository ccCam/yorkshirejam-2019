extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var is_paused = false

func _ready():
	self.hide()

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		if is_paused:
			is_paused = false
			self.hide()
			get_tree().paused = false
		else:
			is_paused = true
			self.show()
			get_tree().paused = true


func _on_btnResume_pressed():
	is_paused = false
	self.hide()
	get_tree().paused = false


func _on_btnMenu_pressed():
	get_tree().change_scene("res://Scenes/Menu.tscn")
	get_tree().paused = false
