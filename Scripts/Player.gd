extends KinematicBody2D

const RUN_SPEED = 500
const JUMP_SPEED = -800
const GRAVITY = 2000

var velocity = Vector2()

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func get_input():
	velocity.x = 0
	var right = Input.is_action_pressed("ui_right")
	var left = Input.is_action_pressed("ui_left")
	var jump = Input.is_action_just_pressed("ui_up")
	
	if is_on_floor() and jump:
		velocity.y = JUMP_SPEED
		
	if right:
		velocity.x += RUN_SPEED
	if left:
		velocity.x -= RUN_SPEED
	
	
func _physics_process(delta):
	velocity.y += GRAVITY * delta
	get_input()
	velocity = move_and_slide(velocity, Vector2(0, -1))
